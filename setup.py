from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='transformat',
    version='0.0.1',
    author='Matrixcoffee',
    author_email='Matrixcoffee@users.noreply.github.com',
    packages=['transformat'],
    url='https://gitlab.com/Matrixcoffee/transformat/',
    license='LICENSE',
    description='a general purpose
formatted text storage and transformation library',
    long_description=long_description,
    long_description_content_type="text/markdown",
)
